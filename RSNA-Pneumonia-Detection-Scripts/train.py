import pandas as pd
import argparse
import numpy as np
import os, time

from tensorflow.keras import layers
from tensorflow.keras.applications import DenseNet201, InceptionV3
from tensorflow.keras.callbacks import Callback, ModelCheckpoint, ReduceLROnPlateau, TensorBoard
from tensorflow.keras.preprocessing.image import ImageDataGenerator

from tensorflow.keras.layers import Dense
from tensorflow.keras import Sequential
from tensorflow.keras import Model
from tensorflow.keras import backend as K
from tensorflow.keras.preprocessing.image import load_img, img_to_array

from tensorflow.keras.optimizers import Adam
from sklearn.utils import class_weight
from sklearn.metrics import accuracy_score, confusion_matrix, classification_report, roc_auc_score, auc, roc_curve

def generate_batches(train_df, val_df, test_df, batch_sizes):
    
    """ 
    Description - Loads data and returns batches of images
    :type train_df: dataframe
    :param train_df: dataframe containing training data
    
    :type val_df: dataframe
    :param val_df: dataframe containing validation data
    
    :type test_df: dataframe
    :param test_df: dataframe containing test data
    
    :type batch_sizes: int
    :param batch_sizes: batch size of generators
    
    :rtype: DataFrameIterator
    """
    train_gen = ImageDataGenerator(
        shear_range=0.2,
        zoom_range=0.2,
        rotation_range=20,
        width_shift_range=0.2,
        height_shift_range=0.2,
        horizontal_flip=True)

    val_gen = ImageDataGenerator()

    test_gen = ImageDataGenerator()

    # Loading batch size of images for training and validation
    train_generator=train_gen.flow_from_dataframe(
        dataframe=train_df,
        x_col="patientId",
        y_col="Target",
        batch_size=batch_sizes, #train_df.shape[0],
        seed=42,
        shuffle=True,
        class_mode="categorical",
        target_size=(224,224))

    val_generator=val_gen.flow_from_dataframe(
        dataframe=val_df,
        x_col="patientId",
        y_col="Target",
        batch_size=batch_sizes, #val_df.shape[0],
        seed=42,
        shuffle=False,
        class_mode="categorical",
        target_size=(224,224))

    test_generator=test_gen.flow_from_dataframe(
        dataframe=test_df,
        x_col="patientId",
        y_col="Target",
        batch_size=batch_sizes, #test_df.shape[0],
        seed=42,
        shuffle=False,
        class_mode="categorical",
        target_size=(224,224))

    return train_generator, val_generator, test_generator

def build_model(model_name):
    
    """ 
    Description - Builds the required model
    :type model_name: str
    :param model_name: Name of model to be built

    :rtype: tensorflow model
    """
    if(model_name == "densenet"):
        base_model = DenseNet201(
            weights='imagenet',
            include_top=False,
            input_shape=(224,224,3))

    elif(model_name == "inception"):
        base_model = InceptionV3(
            weights='imagenet',
            include_top=False,
            input_shape=(224,224,3))
    
    x = base_model.output
    x = layers.GlobalAveragePooling2D()(x)
    x = layers.Dropout(0.5)(x)
    x = layers.BatchNormalization()(x)
    pred_layer = layers.Dense(2, activation='softmax')(x)
    model = Model(inputs=base_model.input, outputs=pred_layer)

    model.compile(
        loss='categorical_crossentropy',
        optimizer=Adam(lr=1e-5),
        metrics=['accuracy']
    )

    return model

def train_model(model, num_epochs, train_generator, val_generator, weights_file, class_weights):
    

    """ Description
    :type model: tensorflow model
    :param model: Model to train

    :type num_epochs: int
    :param num_epochs: Number of epochs to perform training

    :type train_generator: DataFrameIterator
    :param train_generator: Training data generator

    :type val_generator: DataFrameIterator
    :param val_generator: Validation data generator

    :type weights_file: str
    :param weights_file: Path to store best weights

    :type class_weights: numpy array
    :param class_weights: Weights per class to perform class-weighting to deal with imbalance

    :raises:

    :rtype: tensorflow model, tensorflow history
    """
    learn_control = ReduceLROnPlateau(monitor='val_accuracy', patience=5, verbose=1,factor=0.2, min_lr=1e-7)
    # Checkpoint to save the best weights
    filepath=os.path.join(weights_file,"/","weights.best.hdf5")
    checkpoint = ModelCheckpoint(filepath, monitor='val_accuracy', verbose=1, save_best_only=True, mode='max')
    history = model.fit_generator(
        train_generator,
        validation_data=val_generator,
        epochs=num_epochs,
        callbacks=[learn_control, checkpoint],
        # class_weight=class_weights
        )
    
    return model, history

def evaluate_model(history, model, val_generator, test_generator, val_df, test_df):
    
    """ 
    Description - Evaluates model performance
    :type history: tensorflow history
    :param history: History callback of trained model
    
    :type model: tensorflow model
    :param model: Trained model
    
    :type val_generator: DataFrameIterator
    :param val_generator: Validation data generator
    
    :type test_generator: DataFrameIterator
    :param test_generator: Test data generator
    
    :type val_df: DataFrame
    :param val_df: Validation dataframe
    
    :type test_df: DataFrame
    :param test_df: Test dataframe
    """
    print("Model Results : ")
    # history_df = pd.DataFrame(history.history)
    # history_df.plot()

    # history_df[['accuracy','val_accuracy']].plot()
    # history_df[['loss', 'val_loss']].plot()

    print("Validation accuracy : ")
    pred_val=model.predict_generator(val_generator,verbose=1)
    print(accuracy_score(val_df['Target'].astype(int), np.argmax(pred_val, axis=1)))

    print("Test Accuracy : ")
    pred_test=model.predict_generator(test_generator,verbose=1)
    print(accuracy_score(test_df['Target'].astype(int), np.argmax(pred_test, axis=1)))

    print("Validation confusion matrix : ")
    print(confusion_matrix(val_df['Target'].astype(int), np.argmax(pred_val, axis=1)))
    print("Test confusion matrix : ")
    print(confusion_matrix(test_df['Target'].astype(int), np.argmax(pred_test, axis=1)))

    val_loss, val_accuracy = model.evaluate(val_generator)
    print("Final Val loss: {:.4f}".format(val_loss))
    print("Final Val accuracy: {:.4f}%".format(val_accuracy * 100))

    test_loss, test_accuracy = model.evaluate(test_generator)
    print("Final Test loss: {:.4f}".format(test_loss))
    print("Final Test accuracy: {:.4f}%".format(test_accuracy * 100))

def main(args):
    
    """ 
    Description - Main function
    :type args: list
    :param args: List of arguments to parse
    """
    train_df = pd.read_csv("train_df.csv")
    val_df = pd.read_csv("val_df.csv")
    test_df = pd.read_csv("test_df.csv")
    train_df['Target'] = train_df['Target'].astype('str')
    val_df['Target'] = val_df['Target'].astype('str')
    test_df['Target'] = test_df['Target'].astype('str')

    train_generator, val_generator, test_generator = generate_batches(train_df, val_df, test_df, 32)
    print("Enter model type to train - 1. Densenet, 2. Inception")
    model_type = int(input())
    if model_type == 1:
        model_name = "densenet"
    elif model_type == 2:
        model_name = "inception"
    else:
        raise Exception("Incorrect model name")

    model = build_model(model_name)
    
    print("Model summary")
    print(model.summary())
    class_weights = class_weight.compute_class_weight('balanced', np.unique(train_df['Target']), train_df['Target'])
    model, history = train_model(model, args.epochs, train_generator, val_generator, args.weightsfolder, class_weights)
    
    ts = time.time()
    model.save(str(os.path.join(args.weightsfolder, model_name+str(ts)+".h5")))
    print("Model saved to : ", os.path.join(args.weightsfolder, model_name, model_name+str(ts)+".h5"))
    evaluate_model(history, model, val_generator, test_generator, val_df, test_df)
    print("Done")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('weightsfolder', metavar="weightsfolder", type=str, help="Folder path to save weights")
    parser.add_argument('epochs', metavar="epochs", type=int, help="Number of epochs to train for")
    args = parser.parse_args()
    main(args)